#include "Crypt.h"
#include "PNWL.h"

namespace PinWheel
{
	// TODO : Encrypt using a 256bit hashed passcode
	// TODO : Threading
	// TODO : Buffering
	
	unsigned __int32 CreateHash(const unsigned __int32 HashType, const char * pPasscode)
	{
		switch (HashType)
		{
		case(HASH_TYPE_NONE) :
			return 0;
			break;
		case(HASH_TYPE_FNV32) :
			return Hash_FNV32(pPasscode);
			break;
		default :
			return 0;
		}
	}

	PRETURN Machine::Encrypt(const char const * pPasscode)
	{
		// Attempt to open the file
		std::ifstream ifile (sInput, std::ios::in | std::ios::binary | std::ios::ate);
		
		// Quit if the file does not open
		if (!ifile.is_open())
			return PRETURN_FILE;

		unsigned __int64 fileSize = ifile.tellg();	// Get the size of the file in bytes
		unsigned __int64 blockCount = ((fileSize & 0xFF) == 0 ? fileSize >> 8 : (fileSize >> 8) + 1); // Determine teh number of blocks necessary
		unsigned __int64 blockSize = blockCount << 8;	// The size of all of the blocks in bytes

		Header_1_0 header;
		header.Initiate(blockCount);
		header._HEADER.Hash = CreateHash(HashType, pPasscode);
		header._HEADER.HashType = HashType;

		char * pMem = reinterpret_cast<char *>(malloc(blockSize));
		Block256 * pBlocks = reinterpret_cast<PinWheel::Block256 *>(pMem);

		// Quit if not enough memory
		if (!pMem)
			return PRETURN_MEMORY;

		// No stub type yet
		header._HEADER.StubType = STUB_TYPE_NONE;

		// Read the entire file into memory
		ifile.seekg(0, std::ios::beg);
		ifile.read(pMem, fileSize);
		ifile.close();

		// Encrypt the file using the raw passcode
		for (int i = 0; i < blockCount; i++)
		{
			pBlocks[i].Forward(pPasscode);
		}
	
		std::ofstream ofile(sOutput, std::ios::out | std::ios::binary | std::ios::trunc);

		// Write the header
		ofile.write(reinterpret_cast<char*>(&header), header._HEADER._HeaderSize);
		// Write the data
		ofile.write(pMem, blockSize);

		ofile.close();

		free(pMem);

		return PRETURN_SUCCESS;

	}

	PRETURN Machine::Decrypt(const char const * pPasscode)
	{
		// Attempt to open the file
		std::ifstream ifile(sInput, std::ios::in | std::ios::binary | std::ios::ate);

		// Quit if the file does not open
		if (!ifile.is_open())
			return PRETURN_FILE;

		unsigned __int64 fileSize = ifile.tellg();	// Get the size of the file in bytes
		if (fileSize < sizeof(HEADER) + 0xFF)
			return PRETURN_FILE;

		// Read the Header
		HEADER * pHeader = reinterpret_cast<HEADER*>( malloc(sizeof(HEADER)) );
		ifile.seekg(0, std::ios::beg);
		ifile.read(reinterpret_cast<char*>(pHeader), sizeof(HEADER));
		
		// Is this a PNWL encrypted file
		if (pHeader->_Type != HEADER_TYPE)
		{
			free(pHeader);
			return PRETURN_TYPE;
		}

		// Quick check to see if we have the correct passcode
		unsigned __int32 hash = CreateHash(pHeader->HashType, pPasscode);

		// Compare hashes
		if (hash != pHeader->Hash)
		{
			free(pHeader);
			return PRETURN_PASSWORD;
		}

		unsigned __int64 blockCount = pHeader->_BlockCount; // Determine teh number of blocks necessary
		unsigned __int64 blockSize = blockCount << 8;		// The size of all of the blocks in bytes

		char * pMem = reinterpret_cast<char *>(malloc(blockSize));
		Block256 * pBlocks = reinterpret_cast<PinWheel::Block256 *>(pMem);

		// Quit if not enough memory
		if (!pMem)
			return PRETURN_MEMORY;

		// Read the blocks into memory
		ifile.seekg(pHeader->_HeaderSize, std::ios::beg);
		ifile.read(pMem, blockSize);
		ifile.close();

		std::string rPasscode(pPasscode);
		std::reverse(rPasscode.begin(), rPasscode.end());

		// Decrypt the file using the raw reversed passcode
		for (int i = 0; i < blockCount; i++)
		{
			pBlocks[i].Reverse(rPasscode.data());
		}

		std::ofstream ofile(sOutput, std::ios::out | std::ios::binary | std::ios::trunc);

		// Write the file
		ofile.write(pMem, blockSize);

		ofile.close();

		free(pMem);

		return PRETURN_SUCCESS;

	}
}