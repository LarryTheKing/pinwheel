#pragma once

#include <string>
#include <fstream>

#include "Schema.h"
#include "Hash.h"

#define PRETURN_UNKNOWN		0xFFFFFFFF

// GOOD

#define PRETURN_SUCCESS		0x00000001

//BAD

#define PRETURN_FAILURE		0x80000000
#define PRETURN_FILE		0x80000001
#define PRETURN_PASSWORD	0x80000002
#define PRETURN_MEMORY		0x80000003
#define PRETURN_TYPE		0x80000004


#define PFAILURE( x )		(x && PRETURN_FAILURE) == true
#define PSUCCESS( x )		(x && PRETURN_FAILURE) == false

namespace PinWheel
{
	typedef		unsigned __int32 PRETURN;

	class Machine
	{
	public:
		std::string			sInput;
		std::string			sOutput;
		unsigned __int32	HashType;
		unsigned __int32	Flags;

	public:
		Machine(void)
			: sInput(""), sOutput(""), HashType(HASH_TYPE_FNV32), Flags(0) {}
	public:
		PRETURN Encrypt(const char const * pPasscode);
		PRETURN Decrypt(const char const * pPasscode);
	};
}