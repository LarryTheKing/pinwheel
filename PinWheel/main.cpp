#pragma once

#include <iostream>
#include <string>
#include <windows.h>
#include "Crypt.h"

PinWheel::Machine machine;

void SetEcho(bool enable = true)
{
#ifdef WIN32
	HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
	DWORD mode;
	GetConsoleMode(hStdin, &mode);

	if (!enable)
		mode &= ~ENABLE_ECHO_INPUT;
	else
		mode |= ENABLE_ECHO_INPUT;

	SetConsoleMode(hStdin, mode);
#else
	struct termios tty;
	tcgetattr(STDIN_FILENO, &tty);
	if (!enable)
		tty.c_lflag &= ~ECHO;
	else
		tty.c_lflag |= ECHO;

	(void)tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

std::string ParseError(PinWheel::PRETURN r)
{
	switch (r)
	{
	case(PRETURN_FILE) :
		return "File error";
		break;
	case(PRETURN_MEMORY) :
		return "Out of memory";
		break;
	case(PRETURN_PASSWORD) :
		return "Password";
		break;
	case(PRETURN_TYPE) :
		return "File type";
		break;
	default :
		return "Unknown";
		break;
	}
	return "Unknown";
}

bool parseArg(const std::string & line)
{
	if (line == "help")
	{
		std::cout << "======== ===== === == = = = = == === ===== ========\nCommands:\n-i\tInput file\n-o\tOutput file\n-h\tHash type\n-e\tEncrypt\n-d\tDecrypt\nexit\tQuits program\n";
		std::cout << "========\nHash Types:\n0\tNo Hash\n4\tFNV32\n";
		return true;
	}
	else if (line == "exit")
	{
		return false;
	}
	else if (line == "-e")
	{
		std::cout << "Password : ";
		SetEcho(false);
		std::string p, b = "";
		std::getline(std::cin, p);
		SetEcho(true);
		std::cout << "\nEncrypt? (y/n) : ";
		std::getline(std::cin, b);
		if (b == "y")
		{
			std::cout << "Encrypting...\n";
			PinWheel::PRETURN preturn = machine.Encrypt(p.data());
			if (preturn == PRETURN_SUCCESS)
				std::cout << "Success\n";
			else
			{
				std::cout << "An error occured : " << ParseError(preturn) << std::endl;
			}
		}
		else
		{
			std::cout << "Cancled\n";
		}
		return true;
	}
	else if (line == "-d")
	{
		std::cout << "Password : ";
		SetEcho(false);
		std::string p, b = "";
		std::getline(std::cin, p);
		SetEcho(true);
		std::cout << "Decrypting...\n";
		PinWheel::PRETURN preturn = machine.Decrypt(p.data());
		if (preturn == PRETURN_SUCCESS)
				std::cout << "Success\n";
		else
		{
				std::cout << "An error occured : " << ParseError(preturn) << std::endl;
		}
		return true;
	}
	else if (line.length() > 3 && line[0] == '-')
	{
		switch (line[1])
		{
			case('i') :
				machine.sInput = line.substr(3, std::string::npos);
				return true;
				break;
			case('o') :
				machine.sOutput = line.substr(3, std::string::npos);
				return true;
				break;
			case('h') :
			{
				switch (line[3])
				{
				case('4') :
					machine.HashType = HASH_TYPE_FNV32;
					break;
				case('0') :
					machine.HashType = HASH_TYPE_NONE;
					break;
				default:
					machine.HashType = HASH_TYPE_NONE;
					std::cout << "Unrecognized type. Type \"help\" for a list of hash types.\n";
					break;
				}
				return true;
			} break;
		}
	}

	std::cout << "Unrecognized command. Type \"help\" for a list of commands.\n";
	return true;
}

int main(int argc, char *argv[])
{
	std::cout << "PinWheel Encryption : Copyright 2014 Laurence King\nType \"help\" for a list of commands.\n";
	
	std::string line;

	while (1)
	{
		std::getline(std::cin, line);
		if (!parseArg(line))
			return 0;
	}

	/*std::string filename;
	std::string password;
	char * memblock = nullptr;

	std::cout << "File To Encrypt: ";
	std::getline(std::cin, filename);

	std::cout << "Encryption Password: ";
	std::getline(std::cin, password);
	std::cout << "Encrypting...\n";
	
	PinWheel::PRETURN preturn = PinWheel::Encrypt(filename.data(), password.data());

	if(preturn == PRETURN_SUCCESS)
		std::cout << "Done\n";
	else
	{
		std::cout << "An error occured!"; return 0;
	}

	std::getchar();

	std::cout << "Decrypting...\n";
	preturn = PinWheel::Decrypt((filename + ".pnwl").data(), password.data());

	if (PSUCCESS(preturn))
		std::cout << "Done\n";
	else
		std::cout << "An error occured!"; return 0;

		*/
	std::getchar();
	return 0;
}