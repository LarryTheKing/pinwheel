//Copyright (C) 2014 Laurence Taher King
//
//Permission is hereby granted, free of charge, to any person obtaining a
//copy of this software and associated documentation files (the "Software"),
//to deal in the Software without restriction, including without limitation
//the rights to use, copy, modify, merge, publish, distribute, sublicense,
//and/or sell copies of the Software, and to permit persons to whom the
//Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included 
//in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#pragma once

#define HEADER_TYPE		0x4C574E50

#include "PNWL.h"

namespace PinWheel
{
	
	struct HEADER
	{
		// Required items in the header, in order
		unsigned	__int32		_Type;			// 'PNWL' Identifies file
		unsigned	__int32		_Version;	// The version of PNWL used
		unsigned	__int64		_HeaderSize;	// The size of the entire 'Header' struct in bytes 
		unsigned	__int64		_BlockCount;	// The number of 'Block256' that make up the encrypted portion of this file

		// Extra data, useful for quick a quick qualification
		unsigned	__int32		HashType;		// The password hashing algorithm used for the 'Hash' ; Zero for no Hash
		unsigned	__int32		Hash;			// A 'quick and dirty' hash of the password

		// A header stub may follow
		unsigned	__int32		StubType;		// An identifier for interpreting stubs
	};


#define STUB_TYPE_NONE			0
#define STUB_TYPE_HEADER_1_0	0x00000001

	struct Header_1_0 
	{
		HEADER _HEADER;

		// Open / non-essential info

		//char	Name[256];		// A block compression of the name of the original file

		void Initiate(unsigned	__int64 blockCount)
		{
			_HEADER._Type = HEADER_TYPE;
			_HEADER._Version = PNWL_VERSION;
			_HEADER._HeaderSize = sizeof(Header_1_0);
			_HEADER._BlockCount = blockCount;
		};
	};
}